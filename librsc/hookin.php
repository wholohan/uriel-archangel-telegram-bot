<?php
	/*
		HookIn class is for processing an incoming Telegram update (webhook), and preparing output(s) to the Telegram
		Bot API when appropriate.
		This class makes use of Storage instances for SQL operations, and ReqOut instances for cURL requests.
		Instantiate the class with the open PDO connection, then call this->run to execute.
		The update object from Telegram is accessed at php://input
		
		Invocation Parameters:
			conn: MyPDO object, the open database connection
		
		Run Parameters:

		Return:
		
		Methods are grouped as follows:
			construction methods - called by __construct(),
			run methods - this->run(), & a method each for message, callbackQuery, and 'lurking',
			service methods - access control, convenience methods for bot command & callbackQuery processing,
			output methods - for preparing & sending all the different types of output to Telegram API,
			bot methods - one for each Bot Command in Telegram (need not be documented in Telegram Client)
	*/

	class HookIn
	{	public function __construct(MyPDO $conn)
		{	$this->conn = $conn;
			$this->update = json_decode(file_get_contents('php://input'),TRUE);
			$this->cQuery = ($this->update['callback_query']) ?: NULL;
			$this->message = ($this->update['message']) ?: NULL;
			$this->setGoodies();
			$this->setPrayerful();
			$this->setVictim();
			$cache = fopen($_SERVER['DOCUMENT_ROOT'].'/lastupdate.cache','wb');
			fwrite($cache, json_encode($this->update));
			fclose($cache);
		}

//construction methods

		protected function setGoodies()
		{	if($this->message['entities'])
			{	$this->goodies = array_column($this->message['entities'],NULL,'type');
				if($this->goodies['bot_command'])
				{	$this->command =	strtolower(
						ltrim(
							substr(
								$this->message['text'],
								$this->goodies['bot_command']['offset'],
								$this->goodies['bot_command']['length']
							),
							'\/'
						)
					);
					$this->command = (strstr($this->command,'@',TRUE)) ?: $this->command;
				}
				if($this->goodies['hashtag'])
				{	$this->hashtag =	strtolower(
						ltrim(
							substr(
								$this->message['text'],
								$this->goodies['hashtag']['offset'],
								$this->goodies['hashtag']['length']
							),
							'#'
						)
					);
				}
			}
		}

		protected function setVictim()
		{	if($this->goodies['mention'])
			{	$this->victim = 	ltrim(substr(
												$this->message['text'],
												$this->goodies['mention']['offset'],
												$this->goodies['mention']['length']),
											'@'
										);
			}	elseif($this->goodies['text_mention'])
			{	$this->victim = $this->goodies['text_mention']['user']['id'];
				$this->freak = '['.$this->goodies['text_mention']['user']['first_name'].'](tg://user?id='.$this->victim.')';
			}	elseif($this->message['from']['username'])
			{	$this->victim = $this->message['from']['username'];
			}	else
			{	$this->victim = $this->message['from']['id'];
				$this->freak = '['.$this->message['from']['first_name'].'](tg://user?id='.$this->victim.')';
			}
			$inst = new Storage($this->conn,'judge');
			$this->judgement = $inst->run([$this->victim]);
		}
		
		protected function setPrayerful()
		{	$this->prayerful = ($this->cQuery ? $this->cQuery['from']['id'] : $this->message['from']['id']);
			$inst = new Storage($this->conn,'patron');
			$this->patron = $inst->run([$this->prayerful]);
		}
		
		protected function setMedia()
		{	if(!$this->hashtag)
			{	$start = $this->goodies['bot_command']['offset']+$this->goodies['bot_command']['length'];
				$this->media['archive'] = 	strtolower(
					trim(
						substr(
							$this->message['text'],
							$start
						)
					)
				);
			}	else
			{	$start = $this->goodies['bot_command']['offset']+$this->goodies['bot_command']['length'];
				$length = $this->goodies['hashtag']['offset'] - $start;
				$this->media['archive'] = 	strtolower(
					trim(
						substr(
							$this->message['text'],
							$start,
							$length
						)
					)
				);
			}
			
			if($this->message)
			{	if($this->message['reply_to_message']['photo'])
				{	$this->media['type'] = 'photo';
					$this->media['file_id'] = $this->message['reply_to_message']['photo'][0]['file_id'];
				}	elseif($this->message['reply_to_message']['sticker'])
				{	$this->media['type'] = 'sticker';
					$this->media['file_id'] = $this->message['reply_to_message']['sticker']['file_id'];
				}	elseif($this->message['reply_to_message']['animation'])
				{	$this->media['type'] = 'animation';
					$this->media['file_id'] = $this->message['reply_to_message']['animation']['file_id'];
				}	elseif($this->message['reply_to_message']['voice'])
				{	$this->media['type'] = 'voice';
					$this->media['file_id'] = $this->message['reply_to_message']['voice']['file_id'];
				}	elseif($this->message['reply_to_message']['video'])
				{	$this->media['type'] = 'video';
					$this->media['file_id'] = $this->message['reply_to_message']['video']['file_id'];
				}	else 
				{	return FALSE;
				}
			} else if($this->cQuery)
			{	if($this->cQuery['message']['photo'])
				{	$this->media['type'] = 'photo';
					$this->media['file_id'] = $this->cQuery['message']['photo'][0]['file_id'];
				}	elseif($this->cQuery['message']['sticker'])
				{	$this->media['type'] = 'sticker';
					$this->media['file_id'] = $this->cQuery['message']['sticker']['file_id'];
				}	elseif($this->cQuery['message']['animation'])
				{	$this->media['type'] = 'animation';
					$this->media['file_id'] = $this->cQuery['message']['animation']['file_id'];
				}	elseif($this->cQuery['message']['voice'])
				{	$this->media['type'] = 'voice';
					$this->media['file_id'] = $this->cQuery['message']['voice']['file_id'];
				}	elseif($this->cQuery['message']['video'])
				{	$this->media['type'] = 'video';
					$this->media['file_id'] = $this->cQuery['message']['video']['file_id'];
				}	else 
				{	return FALSE;
				}
			}
			return TRUE;
		}

// run methods
	
		public function run()
		{	if($this->command)
			{	$this->doCommand();
			}	elseif($this->cQuery)
			{	$this->doCallback();
			}	else 
			{	$this->doLurk();
			}
		}
		
		protected function doCommand()
		{	if(method_exists($this,$this->command)) 
			{	if($this->consultTheScroll()) $this->{$this->command}();
			} 	else 
			{	$this->errMsg();
			}
		}
		
		protected function doCallback()
		{	$this->cGoodies = array_column($this->cQuery['message']['reply_to_message']['entities'],NULL,'type');
			if($this->cGoodies['bot_command'])
			{	$this->cCommand = strtolower(
					ltrim(
						substr(
							$this->cQuery['message']['reply_to_message']['text'],
							$this->cGoodies['bot_command']['offset'],
							$this->cGoodies['bot_command']['length']
						),
						'\/'
					)
				);
				$this->cCommand = (strstr($this->cCommand,'@',TRUE)) ?: $this->cCommand;
			}

			switch($this->cCommand)
			{	case 'baptize':
					$result = $this->christen();
					$this->answerQuery($result);
					break;
				case 'give':
					$this->setMedia();
					$this->vote();
					break;
			}
		}
		
		protected function doLurk()
		{	if($this->judgement)
			{	$this->justice();
			}
		}

// service methods 

		protected function consultTheScroll()
		{	$scroll['peter'] =
			[	'forget',
				'loathe',
				'love'
			];
			
			$scroll['paul'] =
			[	'guesswhat',
				'keep',
			];
			
			$scroll['mary'] =
			[	'give', 'g',
				'praise',
				'drag'
			];
			
			$allowed =
			[	'baptize',
				'help',
				'start'
			];

			switch($this->patron)
			{	case 'peter':
					$allowed = array_merge($allowed,$scroll['peter']);
				case 'paul':
					$allowed = array_merge($allowed,$scroll['paul']);
				case 'mary':
					$allowed = array_merge($allowed,$scroll['mary']);
				break;
			}						
			if(!in_array($this->command,$allowed))
			{	$this->quickMsg('No.');
				error_log($this->prayerful);
				die();
			}
			return TRUE;
		}

		protected function justice()
		{	$inst = new Storage($this->conn,'teatime');
			if($inst->run([$this->victim]))
			{	switch($this->judgement)
				{	case 'love':
						$this->praise();
						break;
					case 'loathe':
						$this->drag();
						break;
				}
			}
		}
		
		protected function christen()
		{	if($this->patron != 'peter') return 'No.';
			$inst = new Storage($this->conn,'baptize');
			try
			{	$inst->run([$this->cQuery['data'],$this->cQuery['message']['reply_to_message']['from']['id']]);
			}	catch (PDOException $e)
			{	return 'No.';
			}
			$object = [ 'message_id' => $this->cQuery['message']['message_id'],
							'chat_id' => $this->cQuery['message']['chat']['id'],
							'parse_mode' => 'Markdown',
							'text' => '[Christened](tg://user?id='.$this->cQuery['message']['reply_to_message']['from']['id'].') to St. '.ucfirst($this->cQuery['data']) ];
			$inst = new ReqOut('editMessageText');
			$inst->run($object);
			return 'Thy will be done';	
		}

		/* 0: user_id,
			1: type ['media'],
			2: reference,
			3: vote 
		*/ 
		protected function vote()
		{	$arr = [	
				$this->prayerful,
				'media',
				$this->media['file_id'],
				$this->cQuery['data']
			];					
			$this->answerQuery(ucfirst($this->cQuery['data']));
			$result = (new Storage($this->conn,'vote'))->run($arr);
			$object = [	'message_id' => $this->cQuery['message']['message_id'],
							'chat_id' => $this->cQuery['message']['chat']['id'] ];
			$tally = (new Storage($this->conn,'tally'))->run([$this->media['file_id']]);
			$object[ 'reply_markup'] = ['inline_keyboard' =>	
													[ //keyboard object
														[ //row object
															[	'text' => "\ud83d\udc4e\ud83c\udffb",
																'callback_data' => 'ugh'
															],
															[	'text' => $tally,
																'callback_data' => 'null'
															],
															[	'text' => '\ud83d\udc4c\ud83c\udffb',
																'callback_data' => 'unh'
															]
														]
													]
												];
			$body = str_replace('\\\\','\\',json_encode($object, JSON_UNESCAPED_UNICODE));
			$inst = new ReqOut('editMessageReplyMarkup');
			$inst->run($body);
		}
		
		protected function giveWhat()
		{	$text = "Give what?";
			$archives = (new Storage($this->conn,'doMeDecimal'))->run();
			foreach ($archives as $name) $keyboard[] = [ '/give '.$name ]; 
			$this->repMsgKeyboard($text,$keyboard);
		}

// output methods

		public function quickMsg($text)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'text' => $text ];
			$inst = new ReqOut('sendMessage');
			$inst->run($object);
		}

		public function quickRepMsg($text)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'text' => $text,
							'reply_to_message_id' => $this->message['message_id'] ];
			$inst = new ReqOut('sendMessage');
			$inst->run($object);
		}

		public function markMsg($text)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'text' => $text, 
							'parse_mode' => 'Markdown' ];
			$inst = new ReqOut('sendMessage');
			$inst->run($object);
		}
		
		public function christenMsg($text)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'text' => 'Christen?',
							'reply_to_message_id' => $this->message['message_id'], 
							'parse_mode' => 'Markdown' ];
			$object['reply_markup'] = 	[ 'inline_keyboard' =>
													[ // keyboard object
														[ // row object
															[ 	'text' => 'Mary',
																'callback_data' => 'mary'
															],
															[	'text' => 'Paul',
															 	'callback_data' => 'paul'
															],
															[	'text' => 'Peter',
															 	'callback_data' => 'peter'	
															]
														]
													]
												];			
			$inst = new ReqOut('sendMessage');
			$inst->run($object);
		}
		
		public function defaultMsg()
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'text' => 'Tremble not, but speak.' ];
			$inst = new ReqOut('sendMessage');
			$inst->run($object);
		}

		public function errMsg()
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'text' => "You seek a gender which does not exist." ];
			$inst = new ReqOut('sendMessage');
			$inst->run($object);
		}

		protected function repMsgKeyboard($text,$keyboard)
		{	$object =
			[	'chat_id' => $this->message['chat']['id'],
				'text' => $text,
				'reply_to_message_id' => $this->message['message_id'],
				'reply_markup' =>
				[	'keyboard' => $keyboard,
					'one_time_keyboard' => TRUE,
					'selective' => TRUE
				] 
			];
			(new ReqOut('sendMessage'))->run($object);
		}
		
		protected function givePhoto($arr)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'photo' => $arr[0],
							'reply_to_message_id' => $this->message['message_id'],
							'parse_mode' => 'Markdown'  ];
			$tally = (new Storage($this->conn,'tally'))->run([$arr[0]]);
			if($arr[1]) $object['caption'] = $arr[1];
			$object[ 'reply_markup'] = ['inline_keyboard' =>	
													[ //keyboard object
														[ //row object
															[	'text' => "\ud83d\udc4e\ud83c\udffb",
																'callback_data' => 'ugh'
															],
															[	'text' => $tally,
																'callback_data' => 'null'
															],
															[	'text' => '\ud83d\udc4c\ud83c\udffb',
																'callback_data' => 'unh'
															]
														]
													]
												];
			$body = str_replace('\\\\','\\',json_encode($object, JSON_UNESCAPED_UNICODE));
			$inst = new ReqOut('sendPhoto');
			$inst->run($body);
		}
		
		protected function giveVideo($arr)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'video' => $arr[0],
							'reply_to_message_id' => $this->message['message_id'],
							'parse_mode' => 'Markdown'  ];
			$tally = (new Storage($this->conn,'tally'))->run([$arr[0]]);
			if($arr[1]) $object['caption'] = $arr[1];
			$object[ 'reply_markup'] = ['inline_keyboard' =>	
													[ //keyboard object
														[ //row object
															[	'text' => "\ud83d\udc4e\ud83c\udffb",
																'callback_data' => 'ugh'
															],
															[	'text' => $tally,
																'callback_data' => 'null'
															],
															[	'text' => '\ud83d\udc4c\ud83c\udffb',
																'callback_data' => 'unh'
															]
														]
													]
												];
			$body = str_replace('\\\\','\\',json_encode($object, JSON_UNESCAPED_UNICODE));
			$inst = new ReqOut('sendVideo');
			$inst->run($body);
		}
		
		protected function giveAnimation($arr)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'animation' => $arr[0],
							'reply_to_message_id' => $this->message['message_id'],
							'parse_mode' => 'Markdown'  ];
			$tally = (new Storage($this->conn,'tally'))->run([$arr[0]]);
			if($arr[1]) $object['caption'] = $arr[1];
			$object[ 'reply_markup'] = ['inline_keyboard' =>	
													[ //keyboard object
														[ //row object
															[	'text' => "\ud83d\udc4e\ud83c\udffb",
																'callback_data' => 'ugh'
															],
															[	'text' => $tally,
																'callback_data' => 'null'
															],
															[	'text' => '\ud83d\udc4c\ud83c\udffb',
																'callback_data' => 'unh'
															]
														]
													]
												];
			$body = str_replace('\\\\','\\',json_encode($object, JSON_UNESCAPED_UNICODE));
			$inst = new ReqOut('sendAnimation');
			$inst->run($body);
		}

		protected function giveVoice($arr)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'voice' => $arr[0], 
							'reply_to_message_id' => $this->message['message_id'],
							'parse_mode' => 'Markdown'  ];
			$tally = (new Storage($this->conn,'tally'))->run([$arr[0]]);
			if($arr[1]) $object['caption'] = $arr[1];
			$object[ 'reply_markup'] = ['inline_keyboard' =>	
													[ //keyboard object
														[ //row object
															[	'text' => "\ud83d\udc4e\ud83c\udffb",
																'callback_data' => 'ugh'
															],
															[	'text' => $tally,
																'callback_data' => 'null'
															],
															[	'text' => '\ud83d\udc4c\ud83c\udffb',
																'callback_data' => 'unh'
															]
														]
													]
												];
			$body = str_replace('\\\\','\\',json_encode($object, JSON_UNESCAPED_UNICODE));
			$inst = new ReqOut('sendVoice');
			$inst->run($body);
		}

		protected function giveSticker($arr)
		{	$object = [ 'chat_id' => $this->message['chat']['id'],
							'sticker' => $arr[0], 
							'reply_to_message_id' => $this->message['message_id'],
							'parse_mode' => 'Markdown'  ];
			$tally = (new Storage($this->conn,'tally'))->run([$arr[0]]);
			if($arr[1]) $object['caption'] = $arr[1];
			$object[ 'reply_markup'] = ['inline_keyboard' =>	
													[ //keyboard object
														[ //row object
															[	'text' => "\ud83d\udc4e\ud83c\udffb",
																'callback_data' => 'ugh'
															],
															[	'text' => $tally,
																'callback_data' => 'null'
															],
															[	'text' => '\ud83d\udc4c\ud83c\udffb',
																'callback_data' => 'unh'
															]
														]
													]
												];
			$body = str_replace('\\\\','\\',json_encode($object, JSON_UNESCAPED_UNICODE));
			$inst = new ReqOut('sendSticker');
			$inst->run($body);
		}

		protected function answerQuery($text)
		{	$object = [ 'callback_query_id' => $this->cQuery['id'],
							'text' => $text ];
			$inst = new ReqOut('answerCallbackQuery');
			$inst->run($object);
		}
		
		protected function intent($action)
		{	$object = [	'chat_id' => $this->message['chat']['id'],
							'action' => $action	];
			$inst = new ReqOut('sendChatAction');
			$inst->run($object);
		}
		
// bot methods
		
		protected function help()
		{	$this->markMsg('Pray and maybe `God` will help you.');
		}
		
		protected function start()
		{	$this->markMsg('_Hark_ sinner. Unto thee `God` deliverance hath sent.');
			$this->defaultMsg();
		}

		protected function love()
		{	if($this->victim)
			{	$inst = new Storage($this->conn,'love');
				$text = $inst->run([$this->victim]);
				$this->{($this->freak ? 'markMsg' : 'quickMsg')}('I love '.($this->freak ?: '@'.$this->victim).'.');
			}	else
			{	$this->quickMsg('@ a user to love they/them.');
			}	
		}

		protected function loathe()
		{	if($this->victim)
			{	$inst = new Storage($this->conn,'loathe');
				$text = $inst->run([$this->victim]);
				$this->{($this->freak ? 'markMsg' : 'quickMsg')}('I loathe '.($this->freak ?: '@'.$this->victim).'.');
			}	else
			{	$this->quickMsg('@ a user to loathe they/them.');
			}	
		}
		
		protected function guesswhat()
		{	if(!$this->hashtag)
			{	$start = ($this->freak) 
						 	? $this->goodies['text_mention']['offset']+$this->goodies['text_mention']['length'] 
						 	: $this->goodies['mention']['offset']+$this->goodies['mention']['length'];

				$text = trim(substr($this->message['text'],$start));
			}	else
			{	$valence = 1;
				$start = ($this->freak) 
							? $this->goodies['text_mention']['offset']+$this->goodies['text_mention']['length'] 
							: $this->goodies['mention']['offset']+$this->goodies['mention']['length'];
				$length = $this->goodies['hashtag']['offset'] - $start;
				$text = trim(substr($this->message['text'],$start,$length));
			}
			$inst = new Storage($this->conn,'guesswhat');
			$inst->run([$this->victim,$text,$valence]);
			$this->{($this->freak ? 'markMsg' : 'quickMsg')}('Got it. '.($this->freak ?: '@'.$this->victim).' '.$text);
		}
		
		protected function keep()
		{	if(!$this->setMedia())
			{	$this->quickRepMsg('Use /keep in reply to some media.');
			}	else
			{	$inst = new Storage($this->conn,'keep');
				$inst->run([$this->media['file_id'],$this->media['type'],$this->media['archive'],$this->hashtag]);
				$this->quickRepMsg('Down the '.$this->media['archive'].' hole.');
			}
		}
		
		protected function give()
		{	if(!$this->hashtag)
			{	$start = $this->goodies['bot_command']['offset']+$this->goodies['bot_command']['length'];
				$archive = strtolower(trim(substr($this->message['text'],$start)));
			}	else
			{	$start = $this->goodies['bot_command']['offset']+$this->goodies['bot_command']['length'];
				$length = $this->goodies['hashtag']['offset'] - $start;
				$archive = strtolower(trim(substr($this->message['text'],$start,$length)));
			}
			if(!$archive)
			{	$this->giveWhat();
			}	else
			{	$inst = new Storage($this->conn,'give');
				$media = $inst->run([$archive]);
				if(!$media)
				{	$this->quickRepMsg("No $archive in the vault.");
				}	else
				{	$this->{'give'.ucfirst($media['type'])}([$media['id']]);
				}
			}
		}
		
		protected function baptize()
		{	$this->christenMsg($this->prayerful);
		}

		protected function praise()
		{	if($this->victim)
			{	$inst = new Storage($this->conn,'praise');
				$text = $inst->run([$this->victim]);
				$this->{($this->freak ? 'markMsg' : 'quickMsg')}(($this->freak ?: '@'.$this->victim).' '.$text);
			}	else
			{	$this->quickMsg('@ a user to praise they/them.');
			}	
		}

		protected function drag()
		{	$inst = new Storage($this->conn,'drag');
			$text = $inst->run([$this->victim]);
			$this->{($this->freak ? 'markMsg' : 'quickMsg')}(($this->freak ?: '@'.$this->victim).' '.$text);	
		}
		
//		alias methods

		protected function g()
		{	$this->give();
		}
	}
?>