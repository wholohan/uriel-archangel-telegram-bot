<?php
	/*
		Storage class is for SQL operations. 
		SQL never lives anywhere but here.
		Instantiate the class with the open PDO connection and the name of one of its protected methods,
		then call this->run to execute
		
		Invocation parameters:
			conn: MyPDO Object, the open database connection,
			method: the name of one of the protected methods in the class, to be called on this->run()

		Run parameters:
			arr: an array to pass into this->method on this->run

		Return:
			mixed, either a value or an array			 
	*/
	
	class Storage
	{	const vote_options = ['unh', 'ugh'];
	
		public function __construct(MyPDO $conn,$method = NULL)
		{	$this->conn = $conn;
			$this->method = $method;
		}
		
		public function run($arr)
		{	if($this->method)
			{	if(method_exists($this,$this->method)) return $this->{$this->method}($arr);
			}
		}
		
		public function patron($arr)
		{	$sql = " SELECT patron
						FROM clergy
						WHERE id=? ";
			return $this->conn->run($sql,$arr)->fetch(PDO::FETCH_COLUMN);
		}
		
		protected function doMeDecimal()
		{	$sql = " SELECT DISTINCT archive
						FROM booty ";
			return $this->conn->run($sql)->fetchAll(PDO::FETCH_COLUMN);
		}
		
		protected function guesswhat($arr)
		{	$sql = " INSERT INTO tea (username, body)
						VALUES (?,?) ";
			$this->conn->run($sql,[$arr[0],$arr[1]]);
			$ind = $this->conn->lastInsertId();
			if($arr[2])
			{	$sql = " UPDATE tea
							SET valence=?
							WHERE ind=? ";
				$this->conn->run($sql,['praise',$ind]);
			}
			return $ind;
		}
		
		protected function baptize($arr)
		{	$sql = " INSERT INTO clergy (patron,id) 
						VALUES (?,?) 
						ON DUPLICATE KEY
						UPDATE patron=VALUES(patron)";
			$this->conn->run($sql,$arr);
		}
		
		/* 0: user_id,
			1: type ['media'],
			2: reference,
			3: vote 
		*/ 
		protected function vote($arr)
		{	$sql = 	"	INSERT INTO democracy(user_id, type, reference)
							VALUES (?,?,?) ";
			$this->conn->run($sql,[$arr[0],$arr[1],$arr[2]]);
			$ballot = $this->conn->lastInsertId();
			if (in_array($arr[3], self::vote_options))
			{	$vote = $arr[3];
				$sql = 	"	UPDATE democracy
								SET $vote=1
								WHERE ind=?";
				$this->conn->run($sql,[$ballot]);
				return TRUE; // democracy in action
			}	else 
			{	$sql = "	DELETE FROM democracy
							WHERE ind=?";
				$this->conn->run($sql,[$ballot]);
				return FALSE; // illegal alien
			}
		}
		
		protected function tally($arr)
		{	$sql = " SELECT (SUM(unh) - SUM(ugh))				
						FROM democracy
						WHERE reference=? ";
			$tally = $this->conn->run($sql,$arr)->fetch(PDO::FETCH_COLUMN);
			return $tally ?: 0;
		}
		
		protected function keep($arr)
		{	$sql = " INSERT INTO booty (id, type, archive, tag)
						VALUES (?,?,?,?) ";
			$this->conn->run($sql,$arr);
		}

		protected function give($arr)
		{	$sql = " SELECT *
						FROM booty
						WHERE archive=?
						ORDER BY used ASC ";
			$result = $this->conn->run($sql,[$arr[0]])->fetch();
			$sql = " UPDATE booty
						SET used=NOW()
						WHERE id=? ";
			$this->conn->run($sql,[$result['id']]);
			return $result;			
		}

		protected function praise($arr)
		{	$sql = " SELECT ind, body
						FROM tea
						WHERE username=?
							AND valence='praise' 
						ORDER BY used ASC ";
			$result = $this->conn->run($sql,[$arr[0]])->fetch();
			$sql = " UPDATE tea
						SET used=NOW()
						WHERE ind=? ";
			$this->conn->run($sql,[$result['ind']]);
			return ($result['body'] ?: 'is not known to be praiseworthy.');			
		}
		
		protected function drag($arr)
		{	$sql = " SELECT ind, body
						FROM tea
						WHERE username=?
							AND valence='drag' 
						ORDER BY used ASC ";
			$result = $this->conn->run($sql,[$arr[0]])->fetch();
			$sql = " UPDATE tea
						SET used=NOW()
						WHERE ind=? ";
			$this->conn->run($sql,[$result['ind']]);
			return ($result['body'] ?: 'is not known to lack virtue.');			
		}

		protected function love($arr)
		{	if(!$this->conn->run("SELECT COUNT(*) FROM damned WHERE username=?",$arr)->fetch(PDO::FETCH_COLUMN))
			{	$sql = " INSERT INTO damned (username,valence)
							VALUES (?,'love') ";
				$this->conn->run($sql,$arr);
			}	else
			{	$sql = " UPDATE damned 
							SET valence='love'
							WHERE username=? ";
				$this->conn->run($sql,$arr);
			}
		}		

		protected function loathe($arr)
		{	if(!$this->conn->run("SELECT COUNT(*) FROM damned WHERE username=?",$arr)->fetch(PDO::FETCH_COLUMN))
			{	$sql = " INSERT INTO damned (username,valence)
							VALUES (?,'loathe') ";
				$this->conn->run($sql,$arr);
			}	else
			{	$sql = " UPDATE damned 
							SET valence='loathe'
							WHERE username=? ";
				$this->conn->run($sql,$arr);
			}
		}
		
		protected function judge($arr)
		{	$sql = " SELECT valence
						FROM damned
						WHERE username=? ";
			return $this->conn->run($sql,$arr)->fetch(PDO::FETCH_COLUMN);
		} 

		protected function freak($arr)
		{	$sql = " SELECT name
						FROM freaks
						WHERE id=? ";
			return $this->conn->run($sql,$arr)->fetch(PDO::FETCH_COLUMN);
		}
		
		protected function teatime($arr)
		{	$sql = " SELECT COUNT(*)
						FROM tea
						WHERE username=?
							AND ADDDATE(used, INTERVAL 3 HOUR) > NOW() ";
			$result = ($this->conn->run($sql,$arr)->fetch(PDO::FETCH_COLUMN)) ? FALSE : TRUE;
			return $result;
		}
	}
?>
