﻿<?php
	class MyPDO extends PDO
	{	public function __construct($dsn, $username = NULL, $password = NULL, $options = [])
	   {	$default_options =
	   	[	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	         PDO::ATTR_EMULATE_PREPARES => false,
	         PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
	      ];
	      $options = array_replace($default_options, $options);
	      parent::__construct($dsn, $username, $password, $options);
	    }
	
	    public function run($sql, $arr = NULL)
	    {	if (!$arr)
	      {	return $this->query($sql);
	      }
	
	      $stmt = $this->prepare($sql);
	      $stmt->execute($arr);
	      return $stmt;
	    }
	}

	$db_name = 'wholohan_bots_uriel';
	$db_username = 'wholohan_uriel';
	$db_password = '=t$_{W;C5$B(';

	try
	{	$conn = new MyPDO("mysql:host=localhost;dbname=$db_name", $db_username, $db_password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}	catch (PDOException $e)
	{	echo "
		Error: Connection failed. Please try again.";
		die();
	}
?>